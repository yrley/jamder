import jamder.structural.*;
import jade.core.behaviours.Behaviour;
import jamder.Organization;
import jamder.agents.GenericAgent;
import jamder.roles.*;
import jamder.behavioural.*;

public class PedagogicAgentRole extends ProactiveAgentRole {
   //Constructor 
   public PedagogicAgentRole (String name, Organization owner, GenericAgent player) {
   super(name, owner, player);
   
   addBelief("pedagogicBeliefs.pl", new Belief("pedagogicBeliefs.pl", "String", ""));
   
   addGoal("assistStudentDiscipline", new LeafGoal("assistStudentDiscipline", "String", ""));
   
   addRight("suggestRelatedDiscipline", new Right());
   addRight("suggestRelatedCourses", new Right());
   addRight("informStudentCourseDescription", new Right());
   addRight("informDisciplineDescToStudent", new Right());
   
   addDuty("relateDisciplines", new Duty());
   addDuty("relateCourses", new Duty());
    
   initialize(); 
   }
}
