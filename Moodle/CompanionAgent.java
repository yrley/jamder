import jamder.behavioural.*;
import jamder.Environment;
import jamder.roles.AgentRole;
import jamder.structural.*;
import java.util.List;
import jamder.agents.*;	

public class CompanionAgent extends ModelAgent {
   //Constructor 
   public CompanionAgent (String name, Environment env, AgentRole agRole) {
     super(name, env, agRole);
     addBelief("learningsBeliefsB", new Belief("learningBeliefsB", "String", ""));
   
     Action compareClasspAc = new Action("compareClassAc", null, null);
     addAction("compareClassAc", compareClasspAc);
     Action showSupportMessageAc = new Action("showSupportMessageAc", null, null);
     addAction("showSupportMessageAc", showSupportMessageAc);
     Action showReinforcementMessageAc = new Action("showReinforcementMessageAc", null, null);
     addAction("showReinforcementMessageAc", showReinforcementMessageAc);
     Action requestCoordinatorActionAc = new Action("requestCoordinatorActionAc", null, null);
     addAction("requestCoordinatorActionAc", requestCoordinatorActionAc);
   
     addPerceive("discussionDifficulties", null);
     addPerceive("studentContentAccess", null);
   }
   
   protected Belief nextFunction(Belief belief, String perception) {
	return learningNextFunction(belief, perception);
   }
   private Belief learningNextFunction(Belief belief, String perception) {
	return null;
   }
}
