import jamder.structural.*;
import jade.core.behaviours.Behaviour;
import jamder.Organization;
import jamder.agents.GenericAgent;
import jamder.roles.*;
import jamder.behavioural.*;

public class CoordinatorAgentRole extends ProactiveAgentRole {
   //Constructor 
   public CoordinatorAgentRole (String name, Organization owner, GenericAgent player) {
   super(name, owner, player);
   addBelief("coordinatorBeliefs", new Belief("coordinatorBeliefs", "String", ""));
   
   addGoal("requestAgentActions", new LeafGoal("requestAgentActions", "String", ""));
   
   addRight("checkAgentActions", new Right());
   addRight("requestAction", new Right());
   
   initialize(); 
   }
}
