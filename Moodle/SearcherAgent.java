import jamder.behavioural.*;
import jamder.Environment;
import jamder.roles.AgentRole;
import jamder.structural.*;
import java.util.List;
import jamder.agents.*;

public class SearcherAgent extends MASMLAgent {
   //Constructor 
   public SearcherAgent (String name, Environment env, AgentRole agRole) {
   super(name, env, agRole);
     addBelief("finderBeliefsB", new Belief("finderBeliefsB", "String", ""));
     
     Goal relatePeopleG = new LeafGoal("relatePeopleG", "Boolean", "false");
     addGoal("relatePeopleG", relatePeopleG);
     Goal relateDocumentsG = new LeafGoal("relateDocumentsG", "Boolean", "false");
     addGoal("relateDocumentsG", relateDocumentsG);
   
     Action searchPeopleAc = new Action("searchPeopleAc", null, null);
     addAction("searchPeopleAc", searchPeopleAc);
     Action showRelatedPeopleAc = new Action("showRelatedPeopleAc", null, null);
     addAction("showRelatedPeopleAc", showRelatedPeopleAc);
     Action searchDocumentsAc = new Action("searchDocumentsAc", null, null);
     addAction("searchDocumentsAc", searchDocumentsAc);
     Action showRelatedDocumenstAc = new Action("showRelatedDocumentsAc", null, null);
     addAction("showRelatedDocumentsAc", showRelatedDocumenstAc);
   
     Plan searchPeopleInformationPlan = new Plan("searchPeopleInformationPlan", relateDocumentsG);
     addPlan("searchPeopleInformationPlan", searchPeopleInformationPlan); 
     Plan searchDocumentInformationPlan = new Plan("searchDocumentInformationPlan", relatePeopleG);
     addPlan("searchDocumentInformationPlan", searchDocumentInformationPlan); 
   }
   public void percept(String perception) { }
}
