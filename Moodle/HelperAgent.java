import jamder.behavioural.*;
import jamder.Environment;
import jamder.roles.AgentRole;
import jamder.structural.*;
import java.util.List;
import jamder.agents.*;

public class HelperAgent extends ReflexAgent {

   //Constructor 
   public HelperAgent (String name, Environment env, AgentRole agRole) {
   super(name, env, agRole);
   
   Action showForumTipsAc = new Action("showForumTipsAc", null, null);
   addAction("showForumTipsAc", showForumTipsAc);
   Action showParticipantTipsAc = new Action("showParticipantTipsAc", null, null);
   addAction("showParticipantTipsAc", showParticipantTipsAc);
   Action showCalendarTipsAc = new Action("showCalendarTipsAc", null, null);
   addAction("showCalendarTipsAc", showCalendarTipsAc);
   Action showNewEventTipsAc = new Action("showNewEventTipsAc", null, null);
   addAction("showNewEventTipsAc", showNewEventTipsAc);
   Action showPartinersRecentActivitiesTipsAc = new Action("showPartinersRecentActivitiesTipsAc", null, null);
   addAction("showPartinersRecentActivitiesTipsAc", showPartinersRecentActivitiesTipsAc);
   Action showActivateEditionTipsAc = new Action("showActivateEditionTipsAc", null, null);
   addAction("showActivateEditionTipsAc", showActivateEditionTipsAc);
   Action showConfigurationTipsAc = new Action("showConfigurationTipsAc", null, null);
   addAction("showConfigurationTipsAc", showConfigurationTipsAc);
   Action showFunctionDesignTipsAc = new Action("showFunctionDesignTipsAc", null, null);
   addAction("showFunctionDesignTipsAc", showFunctionDesignTipsAc);
   Action showGradeReportTipsAc = new Action("showGradeReportTipsAc", null, null);
   addAction("showGradeReportTipsAc", showGradeReportTipsAc);
   
   addPerceive("showForumTips", null);
   addPerceive("showParticipantTips", null);
   addPerceive("showCalendarTips", null);
   addPerceive("showNewEventTips", null);
   addPerceive("showPartinersRecentActivitiesTips", null);
   addPerceive("showActivateEditionTips", null);
   addPerceive("showConfigurationTips", null);
   addPerceive("showFunctionDesignTips", null);
   addPerceive("showGradeReportTips", null);
   
   }
}
