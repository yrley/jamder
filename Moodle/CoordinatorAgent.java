import jamder.behavioural.*;
import jamder.Environment;
import jamder.roles.AgentRole;
import jamder.structural.*;
import java.util.List;
import jamder.agents.*;

public class CoordinatorAgent extends MASMLAgent {
   //Constructor 
   public CoordinatorAgent (String name, Environment env, AgentRole agRole) {
     super(name, env, agRole);
     addBelief("coordinatorBeliefsB", new Belief("coordinatorBeliefsB", "String", ""));
     Goal requestAgenstActionsG = new LeafGoal("requestAgentsActionsG", "Boolean", "false");
     addGoal("requestAgentsActionsG", requestAgenstActionsG);
   
     Action checkAgentActionAc = new Action("checkAgentActionAc", null, null);
     addAction("checkAgentActionAc", checkAgentActionAc);
     Action requestActionAc = new Action("requestActionAc", null, null);
     addAction("requestActionAc ", requestActionAc);
   
     Plan requestActionPlan = new Plan("requestActionPlan", requestAgenstActionsG);
     addPlan("requestActionPlan", requestActionPlan); 

     requestActionPlan.addAction("checkAgentActionAc", checkAgentActionAc);
     requestActionPlan.addAction("requestActionAc", requestActionAc);
   }

public void percept(String perception) { }
}
