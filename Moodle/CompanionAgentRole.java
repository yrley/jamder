import jamder.structural.*;
import jade.core.behaviours.Behaviour;
import jamder.Organization;
import jamder.agents.GenericAgent;
import jamder.roles.*;
import jamder.behavioural.*;

public class CompanionAgentRole extends ModelAgentRole {
   //Constructor 
   public CompanionAgentRole (String name, Organization owner, GenericAgent player) {
   super(name, owner, player);
   addBelief("companionBeliefs.pl", new Belief("companionBeliefs.pl", "String", ""));
   addRight("displaySupportMsg", new Right());   
   addDuty("compareClasses", new Duty());
   initialize(); 
   }
}
