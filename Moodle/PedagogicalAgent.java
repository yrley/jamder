import jamder.behavioural.*;
import jamder.Environment;
import jamder.roles.AgentRole;
import jamder.structural.*;
import java.util.List;
import jamder.agents.*;

public class PedagogicalAgent extends GoalAgent {	

   //Constructor 
   public PedagogicalAgent (String name, Environment env, AgentRole agRole) {
   super(name, env, agRole);
   
     addBelief("pedagogicalBeliefsB", new Belief("pedagogicalBeliefsB", "String", ""));
  
     Goal followStudentdisciplinarityG = new LeafGoal("followStudentdisciplinarityG", "Boolean", "");
     addGoal("followStudentdisciplinarityG", followStudentdisciplinarityG);

     Action relateClasssesAc = new Action("relateClasssesAc", null, null);
     Action relateDisciplinesAc= new Action("relateDisciplinesAC", null, null);
     Action indicateRelatedDisciplinesAc= new Action("indicateRelatedDisciplinesAc", null, null);
     Action informStudentClassDescriptionAc= new Action("informStudentClassDescriptionAc", null, null);
     Action informStudentDisciplinesDescriptionAc= new Action("informStudentDisciplinesDescriptionAc", null, null);
     Action requestCoordinatorActionAc = new Action("requestCoordinatorActionAc", null, null);

     addAction("relateClasssesAc", relateClasssesAc);
     addAction("relateDisciplinesAc", relateDisciplinesAc);
     addAction("indicateRelatedDisciplinesAc", indicateRelatedDisciplinesAc);
     addAction("informStudentClassDescriptionAc", informStudentClassDescriptionAc);
     addAction("informStudentDisciplinesDescriptionAc", informStudentDisciplinesDescriptionAc);
     addAction("requestCoordinatorActionAc", requestCoordinatorActionAc);
   
     addPerceive("existentClasses", null);
     addPerceive("classDisciplines", null);
     addPerceive("studentClasses", null);
     addPerceive("studentClassesTheme", null);
     addPerceive("studentDisciplines", null);
     addPerceive("studentDisciplinesTheme", null);
     addPerceive("supportMessage", null);
     addPerceive("locateDocumentsPeople", null);
     addPerceive("funcionalityDifficulties", null);
     addPerceive("createGroup", null);
   
     Plan acompanyStudentPlan = new Plan("acompanyStudentPlan", null);
     addPlan("acompanyStudentPlan", acompanyStudentPlan); 
   }
   
   protected Plan planning(List<Action> actions){
      return null;
   }
      
   protected Goal formulateGoalFunction(Belief belief) {
      return goalFuncPedagogical(belief);
   }
   private Goal goalFuncPedagogical(Belief belief) {
      return null;
   }
   protected List<Action> formulateProblemFunction(Belief belief, Goal goal) {
      return probFuncPedagogical(belief, goal);
   }
   private List<Action> probFuncPedagogical(Belief belief, Goal goal) {
      return null;
   }
   protected Belief nextFunction(Belief belief, String perception) {
      return nextPedagogic(belief, perception);
   }
   private Belief nextPedagogic(Belief belief, String perception) {
      return null;
   }

   public void percept(String perception) { }
}
