import jamder.structural.*;
import jade.core.behaviours.Behaviour;
import jamder.Organization;
import jamder.agents.GenericAgent;
import jamder.roles.*;
import jamder.behavioural.*;

public class GroupAgentRole extends ProactiveAgentRole {
   //Constructor 
   public GroupAgentRole(String name, Organization owner, GenericAgent player) {
   super(name, owner, player);
   addBelief("groupBeliefs.pl", new Belief("groupBeliefs.pl", "String", ""));
   
   addGoal("createGroupByAffinity", new LeafGoal("createGroupByAffinity", "String", ""));
   addGoal("createGroupBtLearningProfile", new LeafGoal("createGroupBtLearningProfile", "String", ""));
   
   addRight("displayFormingGroupTips", new Right());
   addRight("createGroupByProfile", new Right());
   addRight("createGroupByAffinity", new Right());
   addRight("integrateGroupByAffinity", new Right());
   addRight("integrateGroupByProfile", new Right());
   addRight("requestCoordinatorAction", new Right());
   
   initialize(); 
   }
}
