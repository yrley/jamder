import jamder.structural.*;
import jade.core.behaviours.Behaviour;
import jamder.Organization;
import jamder.agents.GenericAgent;
import jamder.roles.*;
import jamder.behavioural.*;

public class HelperAgentRole extends AgentRole {
   //Constructor 
   public HelperAgentRole (String name, Organization owner, GenericAgent player) {
   super(name, owner, player);
   
   addRight("displayForumTips", new Right());
   addRight("displayMateTips", new Right());
   addRight("displayCalendarTips", new Right());
       
   initialize(); 
   }
}
