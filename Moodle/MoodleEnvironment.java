import jamder.environment;
import jamder.Organization;
import jamder.roles.AgentRole;
import jamder.agents.GenericAgent;

public class MoodleEnvironment extends Environment {
  public MoodleEnvironment (String name, String host, String port) {
    super(name, host, port);
    Organization MoodleOrganization = new Organization("MoodleOrganization", this, null);
	addOrganization("MoodleOrganization", MoodleOrganization);
	
    	GenericAgent HelperAgent = new HelperAg("HelperAgent", this, null);
	AgentRole HelperAgentRole = new AgentRole("HelperAgRole", MoodleOrganization, HelperAgent); 
    addAgent("HelperAgent", HelperAgent);
    
    GenericAgent SearcherAgent = new SearcherAgent ("SearcherAgent", this, null);
    AgentRole SearcherAgentRole = new AgentRole("SearcherAgentRole", MoodleOrganization, SearcherAgent);
    addAgent("SearcherAgent", SearcherAgent);
    
    GenericAgent CompanionAgent = new CompanionAgent ("CompanionAgent", this, null);
    AgentRole CompanionAgentRole = new AgentRole("CompanionAgentRole", MoodleOrganization, CompanionAgent);
    addAgent("CompanionAgent", CompanionAgent);
    
    GenericAgent CoordinatorAgent = new CoordinatorAgent("CoordinatorAgent", this, null);
    AgentRole CoordinatorAgentRole = new AgentRole("CoordinatorAgentRole", MoodleOrganization, CoordinatorAgent);
    addAgent("CoordinatorAgent", CoordinatorAgent);
    
    GenericAgent GroupAgent = new GroupAgent ("GroupAgent", this, null);
    AgentRole GroupAgentRole = new AgentRole("GroupAgentRole", MoodleOrganization, GroupAgent);
    addAgent("GroupAgent", GroupAgent);
    
    GenericAgent PedagogicalAgent = new PedagogicAgent("PedagogicalAgent", this, null);
    AgentRole PedagogicAgentRole = new AgentRole("PedagogicAgentRole", MoodleOrganization, PedagogicAgent ); 
    addAgent("PedagogicalAgent", PedagogicalAgent);    
  }
  
  // Additional attributes
  // Additional methods
}
