import jamder.Organization;
import jamder.roles.*;
import jamder.structural.*;
import jamder.behavioural.*;

public class MoodleOrganization extends Organization {
   //Constructor 
   public MoodleOrganization (String name, Environment env, AgentRole agRole, Organization org) {
     super(name, env, agRole, org);
   }
}
