import jamder.behavioural.*;
import jamder.Environment;
import jamder.roles.AgentRole;
import jamder.structural.*;
import java.util.List;
import jamder.agents.*;

public class GroupAgent extends UtilityAgent {
   //Constructor 
  public GroupAgent(String name, Environment env, AgentRole agRole) {
    super(name, env, agRole);
    addBelief("groupCoachBeliefsB", new Belief("groupCoachBeliefsB", "String", ""));
   
    Goal formGroupByAffinityG = new LeafGoal("formGroupByAffinityG ", "Boolean", "false");
    addGoal("formGroupByAffinityG", formGroupByAffinityG);
    Goal formGroupByLearningProfileG = new LeafGoal("formGroupByLearningProfileG ", Boolean, "");
    addGoal("formGroupByLearningProfileG ", formGroupByLearningProfileG);
  
    Action showFormationTipsAc = new Action("showFormationTipsAc", null, null);
    addAction("showFormationTipsAc", showFormationTipsAc);
    Action createGroupByProfileAc = new Action("createGroupByProfileAc", null, null);
    addAction("createGroupByProfileAc", createGroupByProfileAc);
    Action createGroupByAffinityAc = new Action("createGroupByAffinityAc", null, null);
    addAction("createGroupByAffinityAc", createGroupByAffinityAc);
    Action integrateGroupByAffinityAc = new Action("integrateGroupByAffinityAc", null, null);
    addAction("integrateGroupByAffinityAc", integrateGroupByAffinityAc);
    Action requestCoordinatorActionAc = new Action("requestCoordinatorActionAc", null, null);
    addAction("requestCoordinatorActionAc", requestCoordinatorActionAc);
    Action integrateGroupByProfileAc = new Action("integrateGroupByProfileAc", null, null);
    addAction("integrateGroupByProfileAc", integrateGroupByProfileAc);
  
    addPerceive("perceiveGroupFormation", null);
    addPerceive("perceiveThemes", null);
    addPerceive("perceiveUserDisciplines", null);
    addPerceive("perceiveUserClass", null);
    addPerceive("perceiveUserProfile", null);
    addPerceive("supportMessage", null);
    addPerceive("courseTipsMessage", null);
    addPerceive("locateDocumentsPeople", null);
    addPerceive("funciontalityuDifficulties", null);
   
    Plan createGroupHelpPlan = new Plan("createGroupHelpPlan", null);
    addPlan("createGroupHelpPlan", createGroupHelpPlan); 
  }
   
  protected Plan planning(List<Action> actions){
	return null;
  }
   
  protected Belief nextFunction(Belief belief, String perception) {
	return nextFunctionGroup (belief, perception);
  }
  private Belief nextFunctionGroup (Belief belief, String perception) {
	return null;
  }
  protected List<Action> formulateProblemFunction(Belief belief, Goal goal) {
	return probFuncPerceiveGroups(belief, goal);
  }
  private List<Action> probFuncPerceiveGroups(Belief belief, Goal goal) {
	return null;
  }
  protected Goal formulateGoalFunction(Belief belief) {
	return probFuncIntegrateGroups(belief);
  }
  private Goal probFuncIntegrateGroups(Belief belief) {
	return null;
  }
  protected Integer utilityFunction(Action action) {
	return utilFuncIntegrateGroups(action);
  }
  private Integer utilFuncIntegrateGroups(Action action) {
	return 0;
  }
  public void percept(String perception) { }
}
